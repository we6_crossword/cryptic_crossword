HASH, SPACE = '#', "_"

def read_pad_file(path: str) -> list[list[str]]:
    crossword = []
    for line in open(path):
        row = ['#'] + (list(line.strip('\n'))) + ['#']
        crossword.append(row)
    crossword.insert(0, ['#'] * len(crossword[0]))
    crossword.append(['#'] * len(crossword[0]))
    return crossword

def remove_padding(crossword: list) -> list:
    pad_removed = []
    for row in crossword[1:-1]:
        pad_removed.append(row[1:-1])
    return pad_removed

def write_crossword(crossword: list) -> None:
    f = open("result.txt",'w')
    for row in crossword:
         f.write("".join(row) + '\n')
    f.close()


def iterate_csw(crossw: list) -> list:
        num = 1
        for row in range(len(crossw)):
            for col in range(len(crossw[row])):
                 num = assign_number(crossw, row, col, num)
        return crossw

def assign_number(crossw: list, row: int, col: int, num: int) -> None:
    if crossw[row][col] == SPACE:
        if check_next_prev(crossw, row, col) or check_up_down(crossw, row, col) :
                crossw[row][col] = str(num)
                num += 1
    return num

                        
def check_next_prev(crossw: list[list[str]], row: int, col: int) -> bool:
      return  crossw[row][col + 1] == SPACE and crossw[row][col - 1] == HASH

def check_up_down(crossw: list[list[str]], row: int, col: int) -> bool:
      return crossw[row + 1][col] == SPACE and crossw[row - 1][col] == HASH

def number_crossword(path: str) -> None:
    write_crossword(remove_padding(iterate_csw(read_pad_file(path))))   

number_crossword('cwd.txt')